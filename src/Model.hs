{-# LANGUAGE FlexibleInstances #-}

module Model where

import Data.Maybe
import Data.List
import Control.Monad
import Control.Monad.Identity
import Data.Typeable(Typeable)

import Util.Util
import Print


class (Eq a, Show a, Read a, Typeable a) => Id a where

class Id a => BlackId a where
  --temporary id produced by migration (replaced afterwards with normal ids)
  blackId :: a


instance Id Int
instance Id Integer
instance Id a => Id [a]
instance Id Char
instance Id a => Id (Maybe a)
instance Id a => BlackId (Maybe a) where
  blackId = Nothing
instance Id a => BlackId [a] where
  blackId = []

data Node nid cid pid rid lid pt f = Node {
  nodeId :: nid,
  flag :: f,
  concept :: cid,
  properties :: [(pid, pt)],
  references :: [(rid, nid)],
  children :: [(lid, Node nid cid pid rid lid pt f)]
} deriving (Eq, Show)

nodeDefault = Node {
  nodeId = undefined,
  concept = undefined,
  properties = [],
  references = [],
  children = [],
  flag = ()
}

instance Functor (Node nid cid pid rid lid pt) where
  fmap = nodeFlagMap

mapDescendants :: ((Node nid cid pid rid lid pt f) -> (Node nid cid pid rid lid pt f)) -> (Node nid cid pid rid lid pt f) -> (Node nid cid pid rid lid pt f)
mapDescendants f node =
  let
    fNode = f node
  in
    fNode {
      children = (map . fmap) (mapDescendants f) (children fNode)
    }

nodeIdMap :: (nid1 -> nid2) -> Node nid1 cid pid rid lid pt f -> Node nid2 cid pid rid lid pt f
nodeIdMap f node =
  Node {
    flag = flag node,
    concept = concept node,
    nodeId = f (nodeId node),
    properties = properties node,
    children = (map . fmap) (nodeIdMap f) (children node),
    references = (map . fmap) f (references node)
  }

nodeFlagMap :: (f1 -> f2) -> Node nid cid pid rid lid pt f1 -> Node nid cid pid rid lid pt f2
nodeFlagMap f node =
  node {
    flag = f (flag node),
    children = (fmap . fmap) (nodeFlagMap f) (children node)
  }

{--
Not exhausted list of simplifications in comparison to MPS SNodeAPI/structure:
- no specialized links
- no cardinality checks: every child or property has cardinality 0..n
- no difference between interfaces and abstract concepts
--}
data Concept cid pid rid lid = Concept {
  conceptId :: cid,
  conceptAbstract :: Bool,
  conceptRootable :: Bool,
  exactSuperconcepts :: [cid],
  exactConceptProperties :: [pid],
  exactConceptReferences :: [(rid, cid)],
  exactConceptChildren :: [(lid, cid)]
} deriving Show

data SLanguage cid pid rid lid = SLanguage {
  langConcepts :: [Concept cid pid rid lid]
} deriving Show

data SRepository mid cid pid rid lid  = SRepository {
  repoLanguages :: [(mid, SLanguage cid pid rid lid)]
} deriving Show

languages = map snd . repoLanguages

allConcepts repo =
  concat (map langConcepts (languages repo))

findConcept' repo cid =
  filter (\c -> conceptId c == cid) $ allConcepts repo

hasConcept repo cid =
  not $ null $ findConcept' repo cid

findConcept repo cid =
  runIdentity $ findConceptChecking repo cid

findConceptChecking repo cid =
  fromSingleChecking (findConcept' repo cid) ("Searched for concept " ++ show cid ++ " in repository")

isInstanceOf repo node cncpt =
  cncpt `elem` superConcepts repo (concept node)

superConcepts repo cid =
  superConceptsDepth (length $ allConcepts repo) repo cid

superConceptsDepth 0 repo cid = []
superConceptsDepth depth repo cid =
  let
    immediate = exactSuperconcepts $ findConcept repo cid
  in
    cid : concatMap (superConceptsDepth (depth - 1) repo) immediate

conceptProperties :: (Id cid) => SRepository mid cid pid rid lid -> cid -> [pid]
conceptProperties repo concept =
  concatMap (exactConceptProperties . findConcept repo) (superConcepts repo concept)

conceptReferences :: (Id cid) => SRepository mid cid pid rid lid -> cid -> [(rid, cid)]
conceptReferences repo concept =
  concatMap (exactConceptReferences . findConcept repo) (superConcepts repo concept)

conceptChildren :: (Id cid) => SRepository mid cid pid rid lid -> cid -> [(lid, cid)]
conceptChildren repo concept =
  concatMap (exactConceptChildren . findConcept repo) (superConcepts repo concept)

isSubconcept repo subC superC =
  superC `elem` superConcepts repo subC

descendants node =
  node : (concatMap descendants $ map snd $ children node)

allUnique list =
  null $ list \\ nub list

isValidModel repo roots = do
  mapM_ (isValidRoot repo roots) roots
  checkWithMessage ("duplicated node ids") $
    allUnique (map nodeId (concatMap descendants roots))

cyclicConcepts repo = filter isExtendingSelf cncpts
  where
    cncpts = map conceptId $ allConcepts repo
    repoSize = length cncpts
    isExtendingSelf cid = cid `elem` tail (superConcepts repo cid)

isValidLanguage repo =
  mapM_ (\c -> logError ("cyclic extending concepts:" ++ c)) $ cyclicConcepts repo

isValidRoot repo visibleRoots node = do
  descendantConcepts <- mapM (findConceptChecking repo . concept) (descendants node)
  thisConcept <- findConceptChecking repo $ concept node
  isValidStructurally repo visibleRoots node
  checkWithMessage ("instance of abstract concept detected") $
    all (not . conceptAbstract) descendantConcepts
  checkWithMessage ("concept " ++ show (concept node) ++ " is not rootable") $
    conceptRootable thisConcept

isValidStructurally :: (Id cid, Id pid, Id rid, Id lid, Id nid) => SRepository mid cid pid rid lid -> [Node nid cid pid rid lid pt f] -> Node nid cid pid rid lid pt f
  -> Checking String ()
isValidStructurally repo visibleRoots node = do
   mapM_ (isValidStructurally repo visibleRoots) (map snd $ children node)
   thisConcept <- findConceptChecking repo $ concept node
   mapM_ isValidProperty (properties node)
   mapM_ isValidReference (references node)
   mapM_ isValidChildLink (children node)
 where
   isValidProperty (propId, _) = do
     checkWithMessage ("concept " ++ show (concept node) ++ " does not contain property " ++ show (propId)) $
       elem propId $ conceptProperties repo (concept node)
   isValidReference (refId, targetId) = do
     (resolvedTarget, targetConcept) <- return (,) <*>
       (resolveNode visibleRoots targetId) <*>
       (checkInterruptible ("concept " ++ show (concept node) ++ " does not contain reference " ++ show (refId)) $
         lookup refId $ conceptReferences repo (concept node))
     checkWithMessage ("Concept " ++ show (concept resolvedTarget) ++ " cannot be target of reference " ++ show refId) $
       elem targetConcept $ superConcepts repo (concept resolvedTarget)
   isValidChildLink (linkId, child) = do
     targetConcept <- checkInterruptible ("concept " ++ show (concept node) ++ " does not contain link " ++ show (linkId)) $
       lookup linkId $ conceptChildren repo (concept node)
     checkWithMessage ("Concept " ++ show (concept child) ++ " cannot be target of link " ++ show linkId) $
       elem targetConcept $ superConcepts repo (concept child)




getProperty prop node =
  fromSingle (getPropertyMultiple prop node) ("Searched for property " ++ show prop ++ " in node")
getPropertyBoolean prop node =
  case getPropertyMultiple prop node of
    ["True"] -> True
    ["False"] -> False
getPropertyMultiple prop node =
  [ v | (p, v) <- properties node, p == prop]

getReference ref node =
  fromSingle (getReferenceMultiple ref node) ("Searched for reference " ++ show ref ++ " in node")
getReferenceMultiple ref node =
  [ t | (r, t) <- references node, r == ref]

getChildrenMultiple lnk node =
  [ c | (l, c) <- children node, l == lnk]

resolveNode rootList nid =
  fromSingleChecking (filter (\c ->
    nodeId c == nid
  ) (concatMap descendants rootList)) ("Searched node with id " ++ show nid)

resolveNode' rootList nid =
  fromSingle (filter (\c ->
    nodeId c == nid
  ) (concatMap descendants rootList)) ("Searched node with id " ++ show nid)

resolveRoot rootList nid =
  fromSingle (filter (\c ->
    nodeId c == nid
  ) rootList) ("Searched root with id " ++ show nid)

generateConceptStructure
  :: (Id nid) =>
     [Node nid String String String String String ()]
     -> [Concept String String String String]
generateConceptStructure cncpts = map (\conceptNode@(Node nid _ conceptDeclaration props refs chldrn) ->
  if conceptDeclaration /= "ConceptDeclaration"
  then
    error $ "Concept expected, found:\n" ++ ppShow cncpts
  else
    Concept {
      conceptId = getProperty "conceptId" conceptNode,
      conceptAbstract = getPropertyBoolean "myAbstract" conceptNode,
      conceptRootable = getPropertyBoolean "myRootable" conceptNode,
      exactSuperconcepts = map
        (getProperty "conceptId" . resolveRoot cncpts . getReference "targetSuperConcept")
        (getChildrenMultiple "superConcept" conceptNode),
      exactConceptProperties = map
        (getProperty "propertyId")
        (getChildrenMultiple "property" conceptNode),
      exactConceptReferences = map (liftM2 (,)
        (getProperty "referenceId")
        (getProperty "conceptId" . resolveRoot cncpts . getReference "targetRefConcept")
      ) (getChildrenMultiple "reference" conceptNode),
      exactConceptChildren = map (liftM2 (,)
        (getProperty "linkId")
        (getProperty "conceptId" . resolveRoot cncpts . getReference "targetConcept")
      ) (getChildrenMultiple "link" conceptNode)
    }) cncpts




