module Example1 where

import Test.QuickCheck.Gen
import Test.QuickCheck

import Model
import Migration
import Branch
import Generators
import Util.Generators
import Print

repoEx1 :: SRepository () Int Int Int Int
repoEx1 =
  SRepository [
    ((), SLanguage [
      Concept 1 False True  [2] [21,22] [] [(11, 2)],
      Concept 2 False False [] [] [] []
    ])
  ]

migrationEx1 = (repoEx1, MigrationScript [SelectExactInstances 1, ReplaceProperty 21 23, SelectInstances 2, ReplaceLink 11 13])

nodeEx1 = nodeDefault {
  nodeId = 1,
  concept = 1,
  properties = [(23, ()), (22, ())],
  references = [],
  children = [
    (13, nodeDefault {
      nodeId = 2,
      concept = 2,
      properties = [],
      references = [],
      children = []
    })
  ]
}

allBranchesCommute b = do
  included <- reduceBranch (DeepReduceToN 15) b
  return $ allTheSame $ snd included
    where
      allTheSame xs = conjoin $ map (\e -> counterexample (ppShow e ++ "\n not equals to\n" ++ ppShow (head xs)) $ e == head xs) (tail xs)

migrationInnerCommutes repo migration =
  forAllS (resizeS 2 $ genRoot repo Nothing) $ \initial ->
    label ("root size = " ++ show (length $ descendants initial)) $
      allBranchesCommute $
        applyScriptToRootSteps repo migration initial

test1 = uncurry migrationInnerCommutes migrationEx1

demo1 :: Branch (Node Integer Int Int Int Int () ())
demo1 = uncurry applyScriptToRoot migrationEx1 nodeEx1
