module Generators where

import Test.QuickCheck
import Data.Maybe
import Control.Monad
import Control.Monad.State.Lazy
import Control.Monad.State.Lazy

import Model
import Project
import Util.Generators
import Util.Util

isSuperconcept repo superConcept subConcept =
  elem superConcept (superConcepts repo subConcept)

genConceptFromRepo repo Nothing =
  elementsS $ filter (\c -> not (conceptAbstract c) && conceptRootable c) $ allConcepts repo
genConceptFromRepo repo (Just upperBound)
  | 0 == length (filter (\candidate -> not (conceptAbstract candidate) && isSuperconcept repo upperBound (conceptId candidate)) $ allConcepts repo) =
    error $ "cannot select subconcept of " ++ show upperBound ++ " in repo " ++ show (map conceptId $ allConcepts repo)
genConceptFromRepo repo (Just upperBound) =
  elementsS $ filter (\candidate -> not (conceptAbstract candidate) && isSuperconcept repo upperBound (conceptId candidate)) $ allConcepts repo

genRoot :: (Id cid, Show nid, Show pid, Show lid, Show rid, Show mid) => SRepository mid cid pid rid lid -> Maybe cid -> WithId nid Gen (Node nid cid pid rid lid () ())
genRoot repo upperBound = do
  result <- genNode repo upperBound
  setReferences repo [result] result

genTarget :: (Id cid) => SRepository mid cid pid rid lid -> [Node nid cid pid rid lid pt ()] -> cid -> WithId nid Gen (Maybe nid)
genTarget repo model targetConcept = do
  let desc = concatMap descendants model
  let filtered = filter (isSuperconcept repo targetConcept . concept) desc
  case filtered of
    [] -> return Nothing
    _ -> do
      result <- elementsS filtered
      return $ Just $ nodeId result

setReferences :: (Id cid) => SRepository mid cid pid rid lid -> [Node nid cid pid rid lid pt ()] -> Node nid cid pid rid lid pt () -> WithId nid Gen (Node nid cid pid rid lid pt ())
setReferences repo model node = do
  let refLinks = conceptReferences repo (concept node)
  chldren <- (sequence . map sequence) $ (map . fmap) (setReferences repo model) (children node)
  refs <- (sequence . map sequence) $ map (\(refId, targetConcept) -> (refId, genTarget repo model targetConcept)) refLinks
  return $ node {
    references = catMaybes $ map sequence refs,
    children = chldren
  }

setProperties :: (Id cid) => SRepository mid cid pid rid lid -> ((pid, nid) -> Gen pt) -> Node nid cid pid rid lid () () -> Gen (Node nid cid pid rid lid pt ())
setProperties repo propGen node = do
  let props = conceptProperties repo (concept node)
  chldren <- (sequence . map sequence) $ (map . fmap) (setProperties repo propGen) (children node)
  propValues <- mapM (\p -> propGen (p, nodeId node)) props
  return $ node {
    properties = zip props propValues,
    children = chldren
  }

genNode :: (Id cid, Show nid, Show pid, Show lid, Show rid, Show mid) => SRepository mid cid pid rid lid -> Maybe cid -> WithId nid Gen (Node nid cid pid rid lid () ())
genNode repo upperBound = do
  size <- getSizeS
  ndId <- genId
  cncpt <- genConceptFromRepo repo upperBound
  let allChildLinks = conceptChildren repo (conceptId cncpt)
  childLinks <- listOfElementsS allChildLinks
  chldren <- foldM (\(sze, rest) (linkId, targetConcept) -> do
    res <- resizeS sze $ genNode repo $ Just targetConcept
    return (sze, res : rest)
   ) ((size - 1) `div` (length childLinks), []) $ reverse childLinks
  return $ nodeDefault {
    nodeId = ndId,
    concept = conceptId cncpt,
    properties = (zip (conceptProperties repo $ conceptId cncpt) [(),()..]),
    references = [],
    children = (zip (map fst childLinks) $ snd chldren)
  }

genStructureProperty :: Show nid => (String, nid) -> Gen String
genStructureProperty ("propertyId", nid) = return $ "prop" ++ show nid
genStructureProperty ("referenceId", nid) = return $ "ref" ++ show nid
genStructureProperty ("linkId", nid) = return $ "link" ++ show nid
genStructureProperty ("conceptId", nid) = return $ "Concept" ++ show nid
genStructureProperty ("myAbstract", nid) = return "False"
genStructureProperty ("myRootable", nid) = do
  v <- arbitrary
  return $ show (v :: Bool)
genStructureProperty (prop, _) = error $ "unknown structure property: " ++ prop

removeCyclicDependencies modules = do
  adjustedConcepts <- removeCyclicDependencies' (map moduleContent modules)
  return $ map (\(m, cc) -> m {
    moduleContent = cc
  }) $ zip modules adjustedConcepts

removeCyclicDependencies' cncpts = do
  let cyclic = cyclicConcepts $ SRepository [((), SLanguage (generateConceptStructure $ concat cncpts))]
  case cyclic of
    [] -> return cncpts
    _ -> removeCyclicDependencies' =<< (removeCyclicDependency cncpts cyclic)

removeCyclicDependency cncpts cyclic = do
  toRemove <- elements cyclic
  let c = fromSingle (filter (\conceptNode -> getProperty "conceptId" conceptNode == toRemove) $ concat cncpts) "duplicated concept id"
  let c' = c {
    children = filter (\(lid, _) -> lid /= "superConcept") $ children c
  }
  return $ (map . map) (\r -> if nodeId r == nodeId c then c' else r) cncpts

genModules :: (Id cid, Show nid, Show pid, Show lid, Show rid, Show mid, Show pt) => ((pid, nid) -> Gen pt) -> SRepository mid cid pid rid lid -> [Module mid nid cid pid rid lid pt ()] -> WithId mid (WithId nid Gen) [Module mid nid cid pid rid lid pt ()]
genModules genProp repo depModules = do
  rootsWithoutReferences <- lift $ listOfS $ listOfS (genNode repo Nothing)
  rootsWithProperties <- (lift . lift) $ (mapM . mapM) (setProperties repo genProp) rootsWithoutReferences
  rootsWithReferences <- lift $ (mapM . mapM) (setReferences repo (concat rootsWithProperties ++ concatMap moduleContent depModules)) rootsWithProperties
  mids <- replicateM (length rootsWithReferences) genId
  return $ map (\(mid, moduleRoots) -> Module {
    moduleId = mid,
    moduleVersion = 0,
    languageVersion = 0,
    usedLanguages = zip (fst $ unzip $ repoLanguages repo) (cycle [0]),
    moduleDependencies = zip mids (cycle [0]) ++ map (\m -> (moduleId m, moduleVersion m)) depModules,
    moduleContent = moduleRoots,
    refactoringLogs = [],
    migrations = []
  }) $ zip mids rootsWithReferences
