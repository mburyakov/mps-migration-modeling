module Print
 ( ppShow
 , pPrint
 , htmlShow
 , htmlPrint
 , traceShowIdPretty
 ) where

import Text.Show.Pretty
import Debug.Trace
import Data.Tree
import Data.Tree.View
import Data.List
import Data.Char
import Data.Maybe

traceShowIdPretty v = trace (ppShow v) v

valueFromMaybe :: Maybe Value -> Value
valueFromMaybe (Just v) = v
valueFromMaybe Nothing = Con "$NO_VALUE$" []

valueToTree :: Value -> Tree String
valueToTree (Con name values) =
  Node name $ map valueToTree values
valueToTree (List values) =
  Node "[]" $ map valueToTree values
valueToTree (Rec name values) =
  let
    fieldNames = fromMaybe (map fst values) $ sequence $ (map . fmap) lowerize $ map (stripPrefix (lowerize name)) $ map fst values
    lowerize (head:tail) = toLower head : tail
    lowerize [] = []
  in
    Node name $ map (\((rname, cv), rnameStripped) -> case valueToTree cv of (Node l v) -> Node (rnameStripped ++ ": " ++ l) v) $ zip values fieldNames
valueToTree (String value) =
  Node value []
valueToTree (Integer value) =
  Node (show value) []
valueToTree (Neg value) =
  Node ("-" ++ show value) []
valueToTree (Tuple [String label, value]) =
  case valueToTree value of (Node l v) -> Node (label ++ ": " ++ l) v
valueToTree (Tuple values) =
  Node "()" $ map valueToTree values
valueToTree other =
  error (show other)

htmlPrint :: Show a => a -> FilePath -> IO ()
htmlPrint v pth = writeHtmlTree Nothing pth (fmap (\s -> NodeInfo InitiallyCollapsed s s) (valueToTree (valueFromMaybe (reify v))))

htmlShow :: Show a => a -> String
htmlShow v = htmlTree Nothing (fmap (\s -> NodeInfo InitiallyCollapsed s s) (valueToTree (valueFromMaybe (reify v))))