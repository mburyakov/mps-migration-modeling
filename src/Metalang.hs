module Metalang where

import Model
import Migration

structureLanguage :: [NodeS Int]
structureLanguage = [
  nodeDefault {
    nodeId = -1,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "ConceptDeclaration"), ("myAbstract", "False"), ("myRootable", "True")],
    children = [("property", nodeDefault {
      nodeId = -2,
      concept = "PropertyDeclaration",
      properties = [("propertyId", "conceptId")]
    }), ("property", nodeDefault {
      nodeId = -3,
      concept = "PropertyDeclaration",
      properties = [("propertyId", "myAbstract")]
    }), ("property", nodeDefault {
      nodeId = -4,
      concept = "PropertyDeclaration",
      properties = [("propertyId", "myRootable")]
    }), ("link", nodeDefault {
      nodeId = -5,
      concept = "LinkDeclaration",
      properties = [("linkId", "superConcept")],
      references = [("targetConcept", -17)]
    }), ("link", nodeDefault {
      nodeId = -6,
      concept = "LinkDeclaration",
      properties = [("linkId", "property")],
      references = [("targetConcept", -9)]
    }), ("link", nodeDefault {
      nodeId = -7,
      concept = "LinkDeclaration",
      properties = [("linkId", "link")],
      references = [("targetConcept", -11)]
    }), ("link", nodeDefault {
      nodeId = -8,
      concept = "LinkDeclaration",
      properties = [("linkId", "reference")],
      references = [("targetConcept", -14)]
    })]
  },
  nodeDefault {
    nodeId = -9,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "PropertyDeclaration"), ("myAbstract", "False"), ("myRootable", "False")],
    children = [("property", nodeDefault {
      nodeId = -10,
      concept = "PropertyDeclaration",
      properties = [("propertyId", "propertyId")]
    })]
  },
  nodeDefault {
    nodeId = -11,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "LinkDeclaration"), ("myAbstract", "False"), ("myRootable", "False")],
    children = [("property", nodeDefault {
      nodeId = -12,
      concept = "PropertyDeclaration",
      properties = [("propertyId", "linkId")]
    }), ("reference", nodeDefault {
      nodeId = -13,
      concept = "ReferenceDeclaration",
      properties = [("referenceId", "targetConcept")],
      references = [("targetRefConcept", -1)]
    })]
  },
  nodeDefault {
    nodeId = -14,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "ReferenceDeclaration"), ("myAbstract", "False"), ("myRootable", "False")],
    children = [("property", nodeDefault {
      nodeId = -15,
      concept = "PropertyDeclaration",
      properties = [("propertyId", "referenceId")]
    }), ("reference", nodeDefault {
      nodeId = -16,
      concept = "ReferenceDeclaration",
      properties = [("referenceId", "targetRefConcept")],
      references = [("targetRefConcept", -1)]
    })]
  },
  nodeDefault {
    nodeId = -17,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "SuperConceptReference"), ("myAbstract", "False"), ("myRootable", "False")],
    children = [("reference", nodeDefault {
      nodeId = -18,
      concept = "ReferenceDeclaration",
      properties = [("referenceId", "targetSuperConcept")],
      references = [("targetRefConcept", -1)]
    })]
  }]

migrationLanguage :: [NodeS Int]
migrationLanguage = [
  nodeDefault {
    nodeId = -101,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "MigrationScript"), ("myAbstract", "False"), ("myRootable", "True")],
    children = [("link", nodeDefault {
      nodeId = -102,
      concept = "LinkDeclaration",
      properties = [("linkId", "migrationStatement")],
      references = [("targetConcept", -103)]
    })]
  },
  nodeDefault {
    nodeId = -103,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "MigrationStatement"), ("myAbstract", "True"), ("myRootable", "False")],
    children = []
  },
  nodeDefault {
    nodeId = -104,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "SelectExactInstancesStatement"), ("myAbstract", "False"), ("myRootable", "False")],
    children = [("superConcept", nodeDefault {
      nodeId = -105,
      concept = "SuperConceptReference",
      references = [("targetSuperConcept", -103)]
    }), ("reference", nodeDefault {
      nodeId = -106,
      concept = "ReferenceDeclaration",
      properties = [("referenceId", "selectExactInstancesStatementConcept")],
      references = [("targetRefConcept", -1)]
    })]
  },
  nodeDefault {
    nodeId = -107,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "ReplacePropertyStatement"), ("myAbstract", "False"), ("myRootable", "False")],
    children = [("superConcept", nodeDefault {
      nodeId = -108,
      concept = "SuperConceptReference",
      references = [("targetSuperConcept", -103)]
    }), ("reference", nodeDefault {
      nodeId = -109,
      concept = "ReferenceDeclaration",
      properties = [("referenceId", "replacePropertyStatementFrom")],
      references = [("targetRefConcept", -9)]
    }), ("reference", nodeDefault {
      nodeId = -110,
      concept = "ReferenceDeclaration",
      properties = [("referenceId", "replacePropertyStatementTo")],
      references = [("targetRefConcept", -9)]
    })]
  },
  nodeDefault {
    nodeId = -111,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "ReplaceLinkStatement"), ("myAbstract", "False"), ("myRootable", "False")],
    children = [("superConcept", nodeDefault {
      nodeId = -112,
      concept = "SuperConceptReference",
      references = [("targetSuperConcept", -103)]
    }), ("reference", nodeDefault {
      nodeId = -113,
      concept = "ReferenceDeclaration",
      properties = [("referenceId", "replaceLinkStatementFrom")],
      references = [("targetRefConcept", -11)]
    }), ("reference", nodeDefault {
      nodeId = -114,
      concept = "ReferenceDeclaration",
      properties = [("referenceId", "replaceLinkStatementTo")],
      references = [("targetRefConcept", -11)]
    })]
  },
  nodeDefault {
    nodeId = -115,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "ReplaceReferenceStatement"), ("myAbstract", "False"), ("myRootable", "False")],
    children = [("superConcept", nodeDefault {
      nodeId = -116,
      concept = "SuperConceptReference",
      references = [("targetSuperConcept", -103)]
    }), ("reference", nodeDefault {
      nodeId = -117,
      concept = "ReferenceDeclaration",
      properties = [("referenceId", "replaceReferenceStatementFrom")],
      references = [("targetRefConcept", -14)]
    }), ("reference", nodeDefault {
      nodeId = -118,
      concept = "ReferenceDeclaration",
      properties = [("referenceId", "replaceReferenceStatementTo")],
      references = [("targetRefConcept", -14)]
    })]
  },
  nodeDefault {
    nodeId = -119,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "ReplaceToConceptStatement"), ("myAbstract", "False"), ("myRootable", "False")],
    children = [("superConcept", nodeDefault {
      nodeId = -120,
      concept = "SuperConceptReference",
      references = [("targetSuperConcept", -103)]
    }), ("reference", nodeDefault {
      nodeId = -121,
      concept = "ReferenceDeclaration",
      properties = [("referenceId", "replaceToConceptStatementConcept")],
      references = [("targetRefConcept", -1)]
    })]
  }]

migrationLanguageRepo =
  SRepository [
    ("lang.migrations", SLanguage $ take (length migrationLanguage) $ generateConceptStructure (migrationLanguage ++ structureLanguage)),
    ("lang.structure",  SLanguage $ generateConceptStructure structureLanguage)
  ]
