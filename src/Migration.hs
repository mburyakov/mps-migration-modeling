module Migration where

import Control.Monad
import Control.Monad.Trans.Maybe
import Data.Maybe
import Data.List
import Debug.Trace
import Print
import Text.Show.Functions
import Data.List.Split

import Util.Util

import Model
import Branch

data MigrationScript cid pid rid lid pt =
  MigrationScript [MigrationStatement cid pid rid lid pt]
    deriving Show

data MigrationStatement cid pid rid lid pt =
    SelectInstances cid
  | SelectExactInstances cid
  | UnselectAll
  | ReplaceProperty pid pid
  | ReplaceReference rid rid
  | ReplaceLink lid lid
  | ReplaceToConcept cid
    deriving Show

checkMigrated repo (MigrationScript statements) =
  sequence_ . mapM (checkMigratedStatement repo) (chunksOf 2 statements)

checkMigratedStatement repo [SelectExactInstances cold, ReplaceToConcept cnew] node =
  checkWithMessage ("instance of deprecated concept " ++ cold ++ " of concept " ++ concept node) $ not (isInstanceOf repo node cold) || isInstanceOf repo node cnew
checkMigratedStatement repo [_, ReplaceProperty pold pnew] node =
  checkWithMessage ("instance of deprecated property " ++ pold ++ " in concept " ++ concept node) $
    null $ getPropertyMultiple pold node
checkMigratedStatement repo [_, ReplaceReference rold rnew] node =
  checkWithMessage ("instance of deprecated reference " ++ rold ++ " in concept " ++ concept node) $
    null $ getReferenceMultiple rold node
checkMigratedStatement repo [_, ReplaceLink lold lnew] node =
  checkWithMessage ("instance of deprecated link " ++ lold ++ " in concept " ++ concept node) $
    null $ getChildrenMultiple lold node

generateMigration
  :: (Id nid) =>
     [Node nid String String String String String ()]
     -> Node nid String String String String String ()
     -> MigrationScript String String String String pt
generateMigration cncpts migrationScript@(Node nid _ conceptDeclaration props refs chldrn) =
  if conceptDeclaration /= "MigrationScript"
  then
    error $ "Concept expected, found:\n" ++ ppShow cncpts
  else
    MigrationScript $
      map (\statementNode@(Node nid _ statementConcept props refs chldrn) ->
        case statementConcept of
          "SelectExactInstancesStatement" ->
            SelectExactInstances
              ((getProperty "conceptId" . resolveRoot cncpts . getReference "selectExactInstancesStatementConcept") statementNode)
          "ReplacePropertyStatement" ->
            ReplaceProperty
              ((getProperty "propertyId" . resolveNode' cncpts . getReference "replacePropertyStatementFrom") statementNode)
              ((getProperty "propertyId" . resolveNode' cncpts . getReference "replacePropertyStatementTo")   statementNode)
          "ReplaceLinkStatement" ->
            ReplaceLink
              ((getProperty "linkId" . resolveNode' cncpts . getReference "replaceLinkStatementFrom") statementNode)
              ((getProperty "linkId" . resolveNode' cncpts . getReference "replaceLinkStatementTo")   statementNode)
          "ReplaceReferenceStatement" ->
            ReplaceReference
              ((getProperty "referenceId" . resolveNode' cncpts . getReference "replaceReferenceStatementFrom") statementNode)
              ((getProperty "referenceId" . resolveNode' cncpts . getReference "replaceReferenceStatementTo")   statementNode)
          "ReplaceToConceptStatement" ->
            ReplaceToConcept
              ((getProperty "conceptId" . resolveRoot cncpts . getReference "replaceToConceptStatementConcept") statementNode)
          _ -> error ("statement of concept " ++ statementConcept ++ " is not supported")
      ) (getChildrenMultiple "migrationStatement" migrationScript)

-- similar to
-- (return . listToMaybe . catMaybes) <=< (sequence)
-- but doesn't look forward after finding a value
firstJust :: Monad m => [m (Maybe a)] -> m (Maybe a)
firstJust [] = return Nothing
firstJust (x:xs) = do
  res <- x
  case res of
    Nothing -> firstJust xs
    Just v -> return (Just v)


findInstances
  :: (Id nid, MonadVariants b) => (Node nid cid pid rid lid pt f -> Bool)
     -> Node nid cid pid rid lid pt f
     -> (Node nid cid pid rid lid pt f -> Node nid cid pid rid lid pt f)
     -> MaybeT b (nid, Node nid cid pid rid lid pt f)
findInstances predicate node refactoring | predicate node = MaybeT $ do
  let appliedToMe = (nodeId node, refactoring node)
  appliedToChildren <- runMaybeT $ findInstancesTail predicate node refactoring
  case appliedToChildren of
    Just _ ->
      return appliedToChildren
    Nothing ->
      return $ Just appliedToMe
findInstances predicate node refactoring =
  findInstancesTail predicate node refactoring

findInstancesTail
  :: (Id nid, MonadVariants b) => (Node nid cid pid rid lid pt f -> Bool)
     -> Node nid cid pid rid lid pt f
     -> (Node nid cid pid rid lid pt f -> Node nid cid pid rid lid pt f)
     -> MaybeT b (nid, Node nid cid pid rid lid pt f)
findInstancesTail predicate node refactoring = MaybeT $ do
  let chldrn = zip [0..] $ children node
  permutedChildren <- variants "select children order" $ permutations chldrn
  matches <- firstJust $ map (fmap sequence) $ map sequence [(fst child, runMaybeT $ findInstances predicate (snd $ snd child) refactoring) | child <- permutedChildren]
  return $ do
    (childNum, (nid, subTreeResult)) <- matches
    Just (nid, node { children = modifyNth childNum (\(lnk, node) -> (lnk, subTreeResult)) (children node) })

findInstancesBatch
  :: (Id nid, Monad b) => (Node nid cid pid rid lid pt f -> Bool)
     -> Node nid cid pid rid lid pt f
     -> (Node nid cid pid rid lid pt f -> Node nid cid pid rid lid pt f)
     -> b (Node nid cid pid rid lid pt f)
findInstancesBatch predicate node refactoring | predicate node =
  findInstancesBatch predicate (refactoring node) refactoring
findInstancesBatch predicate node refactoring = do
  childMatches <- sequence $ map sequence [(fst child, findInstancesBatch predicate (snd child) refactoring) | child <- children node]
  return $ node { children = childMatches }

applyStatementToNode findInstancesFunc repo (ReplaceToConcept cid2) node =
  findInstancesFunc (isNodeMarked) node (\n -> n { concept = cid2, flag = (False, snd $ flag n) })
applyStatementToNode findInstancesFunc repo (ReplaceProperty prop1 prop2) node =
  findInstancesFunc (isNodeMarked) node (\n -> n { properties = map (\(pid, value) -> (if pid == prop1 then prop2 else pid, value)) (properties n), flag = (False, snd $ flag n) })
applyStatementToNode findInstancesFunc repo (ReplaceLink link1 link2) node =
  findInstancesFunc (isNodeMarked) node (\n -> n { children = map (\(lid, child) -> (if lid == link1 then link2 else lid, child)) (children n), flag = (False, snd $ flag n) })
applyStatementToNode findInstancesFunc repo (ReplaceReference ref1 ref2) node =
  findInstancesFunc (isNodeMarked) node (\n -> n { references = map (\(rid, targetConcept) -> (if rid == ref1 then ref2 else rid, targetConcept)) (references n), flag = (False, snd $ flag n) })
applyStatementToNode findInstancesFunc repo (SelectExactInstances cncpt) node =
  findInstancesFunc (\n -> concept n == cncpt && (not . isNodeMarked) n) node (\n -> n { flag = (True, snd $ flag n) })
applyStatementToNode findInstancesFunc repo (SelectInstances cncpt) node =
  findInstancesFunc (\n ->  isSubconcept repo (concept n) cncpt && (not . isNodeMarked) n) node (\n -> n { flag = (True, snd $ flag n) })
applyStatementToNode findInstancesFunc repo UnselectAll node =
  findInstancesFunc (isNodeMarked) node (\n -> n { flag = (False, snd $ flag n) })

isNodeMarked = fst . flag

applyStatementToRoot :: (MonadVariants b, Show f, Show pt, Eq f, Eq pt, Id nid, Id cid, Id pid, Id rid, Id lid) => SRepository mid cid pid rid lid -> MigrationStatement cid pid rid lid pt -> Node nid cid pid rid lid pt (Bool, f) -> b (Node nid cid pid rid lid pt (Bool, f))
applyStatementToRoot repo migration node = do
  res1 <- applyStatementToRootSteps repo migration node
  res2 <- applyStatementToRootBatch repo migration node
  return (if res1 == res2 then res1 else trace (ppShow res1 ++ "\n not equal to \n " ++ ppShow res2) res1)

applyStatementToRootSteps :: (MonadVariants b, Id nid, Id cid, Id pid, Id rid, Id lid) => SRepository mid cid pid rid lid -> MigrationStatement cid pid rid lid pt -> Node nid cid pid rid lid pt (Bool, f) -> b (Node nid cid pid rid lid pt (Bool, f))
applyStatementToRootSteps repo migration node = do
  possibleFirstStep <- runMaybeT $ applyStatementToNode findInstances repo migration node
  case possibleFirstStep of
    Nothing -> return node
    Just chosenFirstStep -> do
      applyStatementToRootSteps repo migration (snd chosenFirstStep)

applyStatementToRootBatch :: (Monad b, Id nid, Id cid, Id pid, Id rid, Id lid) => SRepository mid cid pid rid lid -> MigrationStatement cid pid rid lid pt -> Node nid cid pid rid lid pt (Bool, f) -> b (Node nid cid pid rid lid pt (Bool, f))
applyStatementToRootBatch repo migration node =
  applyStatementToNode findInstancesBatch repo migration node

applyScriptToRoot repo (MigrationScript statements) initial = do
  result <- foldM (flip $ applyStatementToRootBatch repo) (nodeFlagMap (\f -> (False, f)) initial) statements
  return $ nodeFlagMap (\(False, f) -> f) result

applyScriptToRootSteps repo (MigrationScript statements) initial = do
  result <- foldM (flip $ applyStatementToRoot repo) (nodeFlagMap (\f -> (False, f)) initial) statements
  return $ nodeFlagMap (\(False, f) -> f) result

type NodeS nid = Node nid String String String String String ()

data MoveConceptRefactoring nid = MoveConceptRefactoring {
  moveFrom :: nid,
  updateId :: nid -> nid,
  updateMetaId :: String -> String,
  refactoringOptions :: MoveConceptOptions
} deriving Show

data MoveConceptSupport =
    MigrateManually
  | MigrateManuallyImproved
  | SupportSubconcepts
  | SupportSubconceptsImproved
    deriving Show

data MoveConceptOptions =
    DropMoved
  | KeepMoved MoveConceptSupport
    deriving Show

updateConcept mcr c =
  c {
    nodeId = updateId mcr (moveFrom mcr),
    properties = map (\p ->
      if fst p == "conceptId"
      then
        ("conceptId", updateMetaId mcr (snd p))
      else
        p
    ) (properties c),
    children = map (\l ->
      case fst l of
        "property" ->
          ("property", (snd l) {
            nodeId = updateId mcr (nodeId $ snd l),
            properties = map (\p ->
              if fst p == "propertyId"
              then
                ("propertyId", updateMetaId mcr (snd p))
              else
                p
            ) (properties $ snd l)
          })
        "link" ->
          ("link", (snd l) {
            nodeId = updateId mcr (nodeId $ snd l),
            properties = map (\p ->
              if fst p == "linkId"
              then
                ("linkId", updateMetaId mcr (snd p))
              else
                p
            ) (properties $ snd l)
          })
        "reference" ->
          ("reference", (snd l) {
            nodeId = updateId mcr (nodeId $ snd l),
            properties = map (\p ->
              if fst p == "referenceId"
              then
                ("referenceId", updateMetaId mcr (snd p))
              else
                p
            ) (properties $ snd l)
          })
        "superConcept" ->
          l
        _ ->
          error "unknown child under ConceptDeclaration"
      ) (children c)
  }

applyRefactoringToStructure :: BlackId nid =>  MoveConceptRefactoring nid -> [NodeS nid] -> [NodeS nid]
applyRefactoringToStructure mcr conceptList =
  concatMap (\c ->
    if nodeId c == moveFrom mcr
    then let cNew = updateConcept mcr c in
      case refactoringOptions mcr of
        DropMoved ->
          [cNew]
        KeepMoved _ ->
          [c, cNew {
            children = [("superConcept", nodeDefault {
              nodeId = blackId,
              concept = "SuperConceptReference",
              references = [("targetSuperConcept", nodeId c)]
            })] ++ concatMap (\l ->
              case fst l of
                "superConcept" ->
                  []
                _ ->
                  [l]
              ) (children cNew)
          }]
    else
      [c]
  ) conceptList

extractMigrationFromRefactoringSource mcr conceptList =
  extractMigrationFromRefactoringLog (createLogFromRefactoring mcr conceptList)

extractMigrationFromRefactoringLog
  :: (BlackId nid) =>
     RefactoringLog nid
     -> NodeS nid
extractMigrationFromRefactoringLog refLog =
  let
    selectExactInstancesStatement = nodeDefault {
      nodeId = blackId,
      concept = "SelectExactInstancesStatement",
      references = [("selectExactInstancesStatementConcept", fst $ refactoringLogConcepts refLog)]
    }
    statements =
      [selectExactInstancesStatement] ++ map (\(pold, pnew) -> nodeDefault {
        nodeId = blackId,
        concept = "ReplacePropertyStatement",
        references = [
          ("replacePropertyStatementFrom", pold),
          ("replacePropertyStatementTo", pnew)
        ]
      }) (refactoringLogProperties refLog) ++
      [selectExactInstancesStatement] ++ map (\(rold, rnew) -> nodeDefault {
        nodeId = blackId,
        concept = "ReplaceReferenceStatement",
        references = [
          ("replaceReferenceStatementFrom", rold),
          ("replaceReferenceStatementTo", rnew)
        ]
      }) (refactoringLogReferences refLog) ++
      [selectExactInstancesStatement] ++ map (\(lold, lnew) -> nodeDefault {
        nodeId = blackId,
        concept = "ReplaceLinkStatement",
        references = [
          ("replaceLinkStatementFrom", lold),
          ("replaceLinkStatementTo", lnew)
        ]
      }) (refactoringLogLinks refLog) ++
      [selectExactInstancesStatement, nodeDefault {
        nodeId = blackId,
        concept = "ReplaceToConceptStatement",
        references = [
          ("replaceToConceptStatementConcept", snd $ refactoringLogConcepts refLog)
        ]
      }]
  in
    nodeDefault {
      nodeId = blackId,
      concept = "MigrationScript",
      children = zip (cycle ["migrationStatement"]) statements
    }


extractMigrationFromRefactoring
  :: (BlackId nid) =>
     MoveConceptRefactoring nid
     -> [NodeS nid]
     -> MigrationScript String String String String pt
extractMigrationFromRefactoring mcr conceptList =
  generateMigration conceptList $ extractMigrationFromRefactoringSource mcr conceptList

data RefactoringLog nid =
  MoveConceptRefactoringLog {
    refactoringLogOptions :: MoveConceptOptions,
    refactoringLogConcepts :: (nid, nid),
    refactoringLogProperties :: [(nid, nid)],
    refactoringLogReferences :: [(nid, nid)],
    refactoringLogLinks :: [(nid, nid)]
  }
    deriving Show

refactoringLogIdMap refLog =
     [refactoringLogConcepts refLog]
  ++ refactoringLogProperties refLog
  ++ refactoringLogReferences refLog
  ++ refactoringLogLinks refLog

-- maybeConcepts is Nothing when applying to structure model and (Just concepts) when applying to migration model
-- where concepts are all visible concepts (thus are concepts of this very and other visible languages)
applyRefactoringLog maybeConcepts refLog roots =
  let
    options = refactoringLogOptions refLog
    idMap = refactoringLogIdMap refLog
    updateReference r = case lookup r idMap of
      Just newRefId -> newRefId
      Nothing -> r
    updateReferenceCond rLink sourceNode r = case options of
      DropMoved ->
        updateReference r
      KeepMoved SupportSubconcepts ->
        if
             rLink == "targetSuperConcept" && (nodeId sourceNode) == blackId
          || isJust maybeConcepts
        then
          r
        else
          updateReference r
      KeepMoved SupportSubconceptsImproved ->
        if
             rLink == "targetSuperConcept" && (nodeId sourceNode) == blackId
          || rLink == "targetRefConcept"
          || rLink == "targetConcept"
          || isJust maybeConcepts
        then
          r
        else
          updateReference r
      KeepMoved MigrateManually ->
        if
             rLink == "targetSuperConcept"
          || isJust maybeConcepts
        then
          r
        else
          updateReference r
      KeepMoved MigrateManuallyImproved ->
        if
             rLink == "targetSuperConcept"
          || rLink == "targetRefConcept"
          || rLink == "targetConcept"
          || isJust maybeConcepts
        then
          r
        else
          updateReference r
    updateNode node = node {
      references = map (\(rLink, r) -> (rLink, updateReferenceCond rLink node r)) (references node)
    }
    filterDirectSubConcepts concepts superConcept =
      filter (\cncpt ->
        superConcept `elem` map (getReference "targetSuperConcept") (getChildrenMultiple "superConcept" cncpt)
      ) concepts
    filterAllSubConcepts concepts superConcept =
      let
        ids = transitiveClosureFixPoint (\cncpts -> nub $ cncpts ++ (map nodeId . concatMap (filterDirectSubConcepts concepts)) cncpts) [superConcept]
      in
        filter (\c -> nodeId c `elem` ids) concepts
    additionalMigration extendingConcept =
      let
        parentScript = extractMigrationFromRefactoringLog refLog
        parentStatements = chunksOf 2 $ children parentScript
      in
        parentScript {
          children = concat $ init $ map (\[("migrationStatement", select), ("migrationStatement", replace)] ->
            [("migrationStatement", select {
              references = [("selectExactInstancesStatementConcept", nodeId extendingConcept)]
            }), ("migrationStatement", replace)]
          ) parentStatements
        }
    additionalMigrations concepts = map additionalMigration $ filterAllSubConcepts concepts $ fst (refactoringLogConcepts refLog)
    additionalRoots =
      case maybeConcepts of
        Nothing -> []
        Just concepts ->
          case options of
            KeepMoved SupportSubconcepts ->
              additionalMigrations concepts
            KeepMoved SupportSubconceptsImproved ->
              additionalMigrations concepts
            _ -> []
  in
    (map . mapDescendants) updateNode roots ++ additionalRoots


createLogFromRefactoring
  :: (Id nid) =>
     MoveConceptRefactoring nid
     -> [NodeS nid]
     -> RefactoringLog nid
createLogFromRefactoring mcr conceptList =
  let
    oldConcept = resolveRoot conceptList (moveFrom mcr)
    oldProperties = getChildrenMultiple "property" oldConcept
    oldLinks = getChildrenMultiple "link" oldConcept
    oldReferences = getChildrenMultiple "reference" oldConcept
  in MoveConceptRefactoringLog (refactoringOptions mcr)
       (nodeId oldConcept, updateId mcr $ nodeId oldConcept)
    (map (\oldProperty -> (nodeId oldProperty, updateId mcr $ nodeId oldProperty)) oldProperties)
    (map (\oldReference -> (nodeId oldReference, updateId mcr $ nodeId oldReference)) oldReferences)
    (map (\oldLink -> (nodeId oldLink, updateId mcr $ nodeId oldLink)) oldLinks)

