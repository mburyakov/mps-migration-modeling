module Main where

import Test.QuickCheck
import Control.Monad

import Model
import Branch
import Generators
import Print
import Migration

import Util.Util
import Util.Generators
import Util.CounterExamples

import Example1
import Example2

repoEmpty = SRepository []

sampleMigration' repo migration = do
  initial <- genRoot repo Nothing
  return (initial, applyScriptToRoot repo migration initial)

sampleMigration repo migr = do
  pairs <- sampleS' $ sampleMigration' repo migr
  --putStrLn $ concat $ intersperse "\n" (fmap ppShow pairs)
  putStrLn $ ppShow $ map (snd . fmap (length . branchToList)) pairs


main = do
  visitTestCase test15
  visitTestCase test16
  visitTestCase test17
  visitTestCase test18
  visitTestCase test19
  visitTestCase test20
