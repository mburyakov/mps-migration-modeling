module Branch where

import Test.QuickCheck
import Control.Monad.Trans.Maybe
import Control.Monad
import Control.Monad.Fail
import Data.Maybe (fromJust)
import Control.Monad.State.Lazy

import Util.Util
import Util.Generators

data Branch a = BranchNode {
  branchLabel :: String,
  branchSubbranches :: [Branch a]
} | BranchLeaf {
  branchValue :: a
} deriving Show

instance Functor Branch where
  fmap = (<*>) . pure
  --fmap f (BranchLeaf v) = BranchLeaf (f v)
  --fmap f (BranchNode l xs) = BranchNode l $ map (fmap f) xs

instance Applicative Branch where
  pure = BranchLeaf
  (<*>) = ap
  --liftA2 f (BranchLeaf v) b2 = fmap (f v) b2
  --liftA2 f (BranchNode l xs) b2 = BranchNode l $ map (flip (liftA2 f) b2) xs

instance Monad Branch where
  (BranchLeaf v) >>= f = f v
  (BranchNode l xs) >>= f = BranchNode l $ map (>>= f) xs

-- constraints:
-- variants [value] == return value
-- variants [] == fail
class MonadFail b => MonadVariants b where
  variants :: String -> [a] -> b a
  variantsMaybe :: String -> [a] -> MaybeT b a
  variants label values = do
    (Just res) <- runMaybeT $ variantsMaybe label values
    return res
  variantsMaybe label [] = MaybeT $ return Nothing
  variantsMaybe label values = MaybeT $ fmap Just $ variants label values

instance MonadVariants Branch where
  variants label values = BranchNode label $ map BranchLeaf values

instance MonadVariants m => MonadVariants (StateT a m) where
  variants label values = lift $ variants label values

instance MonadFail Gen where
  fail = error

instance MonadVariants Gen where
  variants label values = elements values

instance MonadFail Branch where
  fail s = BranchNode s []

-- flattens the tree
branchToList (BranchLeaf v) = [v]
branchToList (BranchNode _ xs) = concatMap branchToList xs

listToBranch label xs =
  BranchNode label $ map BranchLeaf xs

-- todo: deprecated, use Gen monad directly
-- more effective than (elements . branchToList) because of laziness
-- also differs from (elements . branchToList) by the probability distribution
elementsB :: Branch a -> Gen a
elementsB (BranchLeaf v) = return v
elementsB (BranchNode _ xs) = do
  ch <- elements xs
  elementsB ch

data ReduceBranchPriorityOptions =
    IncludeEverything    --
  | SelectSingle         -- choose one way leading to one outcome
  | ReduceToN Int        -- track all variants until their amount is less than parameter,
                         -- after that continue each of them to some outcome without branching
  | DeepReduceToN Int    -- track all variants until their amount is less than parameter,
                         -- after that continue each of them except one to some outcome without branching,
                         -- with the remaining do the same
{-
Statistics is intended to estimate branch selection quality.
Ideally, branch tree should be covered uniformly by selected paths.
Meaning of term 'uniform branch tree coverage' needs to be defined formally.
-}
data ReduceStats =
  ReduceStats [Double]
    deriving Show

reduceBranch' opt stats (BranchLeaf v) =
  reduceBranch_ opt stats (Left v)
reduceBranch' opt stats (BranchNode l ls) =
  let
    result = reduceBranch_ opt stats (Right (map (snd . reduceBranch' opt (fromJust $ fst $ fst result)) ls))
  in
    result

reduceBranch_ :: ReduceBranchPriorityOptions -> ReduceStats -> (Either a [Gen [a]]) -> ((Maybe ReduceStats, Gen [Branch a]), Gen [a])
reduceBranch_ IncludeEverything (ReduceStats stats) (Right children) =
  ((Just (ReduceStats (fromIntegral (length children) : stats)), undefined), fmap concat $ sequence children)
reduceBranch_ IncludeEverything (ReduceStats stats) (Left v) =
  ((Just (ReduceStats (1 : stats)), return []), return [v])
{-reduceBranch_ SelectSingle (ReduceStats stats) v children =
  fmap ((,) Nothing . (:) v) $ oneof children
reduceBranch_ (ReduceToN n) (ReduceStats stats) v children =
  if sum stats > fromIntegral n then
    reduceBranch_ SelectSingle (ReduceStats stats) v children
  else
    reduceBranch_ IncludeEverything (ReduceStats stats) v children
-}


--todo: statistics
reduceBranch :: ReduceBranchPriorityOptions -> Branch a -> Gen (Maybe ReduceStats, [a])
reduceBranch IncludeEverything (BranchLeaf v) = return (Just (ReduceStats [1]), [v])
reduceBranch IncludeEverything b  = fmap ((,) Nothing) . return . branchToList $ b
reduceBranch SelectSingle (BranchLeaf v) = return (Just (ReduceStats [1]), [v])
reduceBranch SelectSingle (BranchNode _ xs) = do
  ch <- elements xs
  (Just (ReduceStats stats), res) <- reduceBranch SelectSingle ch
  return (Just (ReduceStats (fromIntegral (length xs):stats)), res)
reduceBranch (ReduceToN n) b = fmap ((,) Nothing) . sequence . map elementsB . reduceN n . pure $ b
reduceBranch (DeepReduceToN n) b =
  fmap ((,) Nothing) . expandSingle (fmap snd . reduceBranch (DeepReduceToN n)) . reduceN n . pure $ b

expandSingle f [BranchLeaf v] = return [v]
expandSingle f xs = do
  n <- choose (0, length xs - 1)
  result <- sequence $ mapExceptNth n (fmap pure . elementsB) f xs
  return $ concat result

reduceN n xs =
  let
    isLeaf (BranchLeaf _) = True
    isLeaf _ = False
    flattenFirstLayer leaf@(BranchLeaf v) = [leaf]
    flattenFirstLayer (BranchNode l xs) = xs
  in
    until ((> n) . length -: liftM2 (||) :- all isLeaf) (concatMap flattenFirstLayer) xs

