module Util.Time where

import System.TimeIt
import System.IO.Unsafe

timeItPure' :: Show a => a -> IO a
timeItPure' v = do
  if 351 == length (show v) then putStrLn "" else return ()
  return v

timeItPure :: Show a => a -> a
timeItPure v =
  unsafePerformIO (timeIt $ timeItPure' v)

timeItShow :: Show a => a -> String
timeItShow v =
  if 351 == length (show v) then " " else ""

