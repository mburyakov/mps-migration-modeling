module Util.Distribution where

import Test.QuickCheck.Gen
import Control.Monad.State.Lazy

data Distribution =
  Tangential Double

genRandom (Tangential p) def = do
  x <- lift $ choose (0.0, 1.0)
  return $ round $ tangentialDistribution p (realToFrac def) x

tangentialDistribution' p c x =
  (unnormalized x - unnormalized 0) / (1 - unnormalized 0)
    where
      unnormalized z =
        2 / pi * atan ((z-c)/p*c)

tangentialDistribution p c x =
  p * tan (pi / 2 * (coeff * (x - 1) + 1)) + c
    where
      coeff = 1 + 2 / pi * atan (c / p)
