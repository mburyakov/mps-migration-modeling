{-# LANGUAGE TypeFamilies, FlexibleInstances #-}

module Util.CounterExamples where

import Test.QuickCheck.Counterexamples
import System.IO
import qualified Test.QuickCheck as QC
import Control.Exception

import Util.Generators
import Util.Util
import Print

visitTestCase
  :: (Show a, Show b, Show c) =>
     TestCase a b c m (Checking String ()) -> IO ()
visitTestCase testcase = do
  (counterExample, result) <- quickCheckWithResult stdArgs (onProperty (setSize 3) $ evaluateTestCase testcase)
  if isSuccess result then
    do
      ex <- generate $ resize 5 $ evaluateTestCase testcase
      let filename = testCaseName testcase ++ ".example.html"
      withFile filename WriteMode (\h -> do
        putStrLn $ "Writing example to " ++ filename
        hSetEncoding h utf8
        hPutStrLn h (htmlShow ex)
       )
  else case counterExample of
    Just cEx -> do
      let filename = testCaseName testcase ++ ".counterexample.html"
      withFile filename WriteMode (\h -> do
        putStrLn $ "Writing counterexample to " ++ filename
        hSetEncoding h utf8
        hPutStrLn h $ htmlShow cEx
       )
    Nothing -> do
      ex <- generate $ resize 5 $ evaluateTestCase testcase
      let filename = testCaseName testcase ++ ".exceptionexample.html"
      withFile filename WriteMode (\h -> do
        putStrLn $ "Writing exception example to " ++ filename
        hSetEncoding h utf8
        hPutStrLn h $ htmlShow $ testCaseEvaluatedInitial ex
       )

conjoin :: Testable prop => [prop] -> PropertyOf [Counterexample prop]
conjoin props =
  MkProperty (\k -> QC.conjoin (map (\prop -> (unProperty . property) prop (\x -> k [x])) props))

instance Testable (TestCaseEvaluated a b c m (Checking String ())) where
  type Counterexample (TestCaseEvaluated a b c m (Checking String ())) = TestCaseEvaluated a b c m [String]
  property tce = fmap (\checked -> tce {
    testCaseEvaluatedChecking = filter (not . null . snd) $ (map . fmap) checkingErrors (testCaseEvaluatedChecking tce)
  }) $ property $ QC.conjoin $ map snd (testCaseEvaluatedChecking tce)
