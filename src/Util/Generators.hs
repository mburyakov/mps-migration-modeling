{-# LANGUAGE FlexibleInstances #-}

module Util.Generators where

import Test.QuickCheck hiding (reason)
import Test.QuickCheck.Random
import Test.QuickCheck.Property
import Test.QuickCheck.Gen
import Control.Monad.State.Lazy
import System.IO

import Util.Distribution
import Util.Util
import Print

type WithId a = StateT [a]

genId :: Monad m => WithId a m a
genId = do
  s <- get
  put (tail s)
  return (head s)
withNodeId :: (Monad m) => WithId Int m a -> m a
withNodeId g = evalStateT g startIds
withNodeIdJust :: (Monad m) => WithId (Maybe Int) m a -> m a
withNodeIdJust g = evalStateT g $ map Just startIds
withModuleId :: (Monad m) => WithId String m a -> m a
withModuleId g = evalStateT g startMids
class Incrementable s where
  increment :: s -> s
instance Incrementable Int where
  increment = (+1)
instance Incrementable s => Incrementable [s] where
  increment (h : t) = (increment h : t)
withIds :: (Incrementable s, Monad m) => s -> WithId s m a -> m a
withIds s g = evalStateT g $ iterate increment s
startIds = [1..]
startMids = map (\i -> "module" ++ show i) [1..]
sampleS g = sample $ evalStateT g startIds
sampleSS g = sampleS $ evalStateT g startMids
generateS g = generate $ evalStateT g startIds
generateSS g = generateS $ evalStateT g startMids
sampleS' g = sample' $ evalStateT g startIds
sampleSS' g = sampleS' $ evalStateT g startMids
getSizeS :: MonadTrans t => t Gen Int
getSizeS = do
  lift getSize
vectorOfS n gen = do
  replicateM n gen
listOfS gen = do
  n <- getSizeS
  k <- lift $ choose (0, n)
  vectorOfS k gen
elementsS :: MonadTrans t => [a] -> t Gen a
elementsS =
  lift . elements
listOfElementsS [] = return []
listOfElementsS xs = do
  n <- getSizeS
  size <- genRandom (Tangential 0.1) (1.2 * realToFrac (min n $ length xs))
  case n of
    0 ->
      return []
    _ ->
      vectorOfS size (elementsS xs)
sublistOfS :: MonadTrans t => [a] -> t Gen [a]
sublistOfS =
  lift . sublistOf

resizeS n =
  mapStateT $ resize n
resizeSS n =
  mapStateT $ resizeS n
scaleS f g = do
  n <- lift $ getSize
  resizeS (f n) g

forAllS g = forAll (evalStateT g startIds)

quickCheckLight p = quickCheckWith (stdArgs {
  replay = Just (mkQCGen 2, 0),
  maxSuccess = 11,
  maxSize = 3
}) p

propertyOnce mp = MkProperty (unProperty . property =<< mp)

instance Testable v => Testable (Checking String v) where
  property = either (\s -> property $ failed { reason = s }) property . fromCheckingToEither

data TestCaseEvaluated a b c m e  = TestCaseEvaluated {
  testCaseEvaluatedStatic :: a,
  testCaseEvaluatedGenerated :: b,
  testCaseEvaluatedInitial :: c,
  testCaseEvaluatedProcessing :: [(String, c)],
  testCaseEvaluatedChecking :: [(String, e)]
}
  deriving Show

evaluateTestCase :: TestCase a b c m e -> Gen (TestCaseEvaluated a b c m e)
evaluateTestCase testcase = do
  let static = testCaseStatic testcase
  generated <- testCaseGenerated testcase static
  let initial = testCaseInitial testcase static generated
  processed <- testCaseProcess testcase static initial
  let checked = testCaseCheck testcase processed
  return TestCaseEvaluated {
           testCaseEvaluatedStatic = static,
           testCaseEvaluatedGenerated = generated,
           testCaseEvaluatedInitial = initial,
           testCaseEvaluatedProcessing = processed,
           testCaseEvaluatedChecking = checked
         }

instance Testable e => Testable (TestCaseEvaluated a b c m e) where
  property tce = conjoin $ map snd (testCaseEvaluatedChecking tce)

data TestCase a b c m e = TestCase {
  testCaseName :: String,
  testCaseStatic :: a,
  testCaseGenerated :: a -> Gen b,
  testCaseInitial :: a -> b -> c,
  testCaseProcess :: a -> c -> Gen [(String, c)],
  testCaseCheck :: [(String, c)] -> [(String, e)]
}

instance (Show a, Show b, Show c) => Testable (TestCase a b c m (Checking String ())) where
  property testcase =
   let static = testCaseStatic testcase
   in
    forAll (testCaseGenerated testcase static) (\generated -> do
      let initial = testCaseInitial testcase static generated
      let filename = testCaseName testcase ++ ".counterexample.html"
      processed <- testCaseProcess testcase static initial
      return $ foldl1 (.&&.) $ map (\(label, prop) -> whenFail (withFile filename WriteMode (\h -> do
        putStrLn $ "Writing counterexample to " ++ filename
        hSetEncoding h utf8
        hPutStrLn h $ htmlShow (("static", testCaseStatic testcase), ("generated", generated), ("initial", initial), ("processing", processed), (label, checkingErrors prop))
       )) prop) $ testCaseCheck testcase processed
    )

setSize :: Testable prop => Int -> prop -> Property
setSize s = property . resize s . unProperty . property