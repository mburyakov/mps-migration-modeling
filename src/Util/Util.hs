{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, GeneralizedNewtypeDeriving #-}

module Util.Util where

import Text.Read(readEither)
import Data.Typeable(Typeable, typeOf)
import Control.Monad hiding (fail)
import Control.Monad.Fail
import Control.Monad.Identity hiding (fail)
import Prelude hiding (fail)
import Data.List.NonEmpty(NonEmpty((:|)), toList)
import Test.QuickCheck.Property
import Test.QuickCheck.Gen
import Data.String

infixr 0 -:, :-
data Infix f y = f :- y
x -:f:- y = x `f` y

modifyNth :: Int -> (a -> a) -> [a] -> [a]
modifyNth n f xs =
  let
      (p, l) = splitAt n xs
    in
      p ++ [f $ head l] ++ tail l

mapExceptNth :: Int -> (a -> b) -> (a -> b) -> [a] -> [b]
mapExceptNth n f g xs =
  map visit $ modifyNth n (fmap not) (map (flip (,) False) xs)
    where
      visit (v, False) = f v
      visit (v, True) = g v

transitiveClosureFixPoint :: Eq a => (a -> a) -> a -> a
transitiveClosureFixPoint f init =
  if init == f init
  then
    init
  else
    transitiveClosureFixPoint f (f init)

readVerbose x = case readEither x of
  Right res -> res
  Left err -> error (err ++ ": " ++ show x ++ " as " ++ show (typeOf (readVerbose x)))


data Checking e v = Checking {
  checkingErrors :: [e],
  checkingValue :: (Maybe v)
}
  deriving Show

instance Functor (Checking e) where
  fmap = (<*>) . pure

instance Applicative (Checking e) where
  pure = Checking [] . Just
  (<*>) (Checking errs1 v1) (Checking errs2 v2) = (Checking (errs1 ++ errs2) (v1 <*> v2))

instance IsString e => MonadFail (Checking e) where
  fail msg = throwError (fromString msg)

instance Monad (Checking e) where
  Checking errs Nothing  >>= f =
    Checking errs Nothing
  Checking errs (Just v) >>= f =
    let
      res = f $! v
    in
      res {
        checkingErrors = errs ++ checkingErrors res
      }
  Checking errs _ >> res =
    res {
      checkingErrors = errs ++ checkingErrors res
    }

checkWithMessage :: e -> Bool -> Checking e ()
checkWithMessage _   True  = return ()
checkWithMessage err False = logError err

-- Interrupts computation. To fork interruptible computation branch use forkCheck
checkInterruptible :: e -> Maybe a -> Checking e a
checkInterruptible err Nothing = throwError err
checkInterruptible _ (Just val) = return val

fromSingle :: [a] -> String -> a
fromSingle [x] _ = x
fromSingle [] message = error (message ++ ", not found")
fromSingle _ message = error (message ++ ", multiple found")

instance MonadFail Identity where
  fail msg = error msg

fromSingleChecking :: MonadFail m => [a] -> String -> m a
fromSingleChecking [x] _ = return x
fromSingleChecking [] message = fail (message ++ ", not found")
fromSingleChecking _ message = fail (message ++ ", multiple found")

logError err   = Checking [err] (Just ())
throwError err = Checking [err] (Nothing)

fromCheckingToEither :: Checking String a -> Either String a
fromCheckingToEither (Checking [] (Just v)) = Right v
fromCheckingToEither (Checking [] Nothing) = Left "Checking failed with no error specified."
fromCheckingToEither (Checking errs (Just v)) = Left $ "Checking failed:" ++ concat (map ("\n"++) errs)
fromCheckingToEither (Checking errs Nothing) = Left $ "Checking failed:" ++ concat (map ("\n"++) errs) ++ "\nException thrown."

fromChecking :: Checking String a -> a
fromChecking v =
  case fromCheckingToEither v of
    Right r -> r
    Left e -> error e
