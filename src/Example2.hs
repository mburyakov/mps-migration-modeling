{-# LANGUAGE TupleSections #-}
module Example2 where

import Test.QuickCheck
import Control.Monad(liftM,liftM2)
import Control.Monad.Identity
import Control.Monad.Trans(lift)

import Model
import Migration
import Project
import Metalang
import Branch
import Util.Generators
import Util.Util
import Generators
import Print
import Util.CounterExamples

import Util.Time
import Debug.Trace


instance Num a => Num (Maybe a) where
  (+) = liftM2 (+)
  (*) = liftM2 (*)
  abs = liftM abs
  signum = liftM signum
  fromInteger = Just . fromInteger
  negate = liftM negate

languageEx2 :: [NodeS Int]
languageEx2 = [
  nodeDefault {
    nodeId = 1,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "MovingConcept"), ("myAbstract", "False"), ("myRootable", "True")],
    children = [("property", nodeDefault {
      nodeId = 2,
      concept = "PropertyDeclaration",
      properties = [("propertyId", "hisProperty")]
    }), ("link", nodeDefault {
      nodeId = 3,
      concept = "LinkDeclaration",
      properties = [("linkId", "hisLink")],
      references = [("targetConcept", 1)]
    }), ("reference", nodeDefault {
      nodeId = 4,
      concept = "ReferenceDeclaration",
      properties = [("referenceId", "hisReference")],
      references = [("targetRefConcept", 1)]
    })]
  },
  nodeDefault {
    nodeId = 5,
    concept = "ConceptDeclaration",
    properties = [("conceptId", "ExtendingConcept"), ("myAbstract", "False"), ("myRootable", "True")],
    children = [("superConcept", nodeDefault {
      nodeId = 6,
      concept = "SuperConceptReference",
      references = [("targetSuperConcept", 1)]
    })]
  }]

nodeEx2 :: NodeS Int
nodeEx2 = nodeDefault {
  nodeId = 1,
  concept = "MovingConcept",
  properties = [("hisProperty", "aValue")],
  children = [("hisLink", nodeDefault {
    nodeId = 2,
    concept = "MovingConcept",
    properties = [],
    children = []
  })]
}

coreRepository = SRepository [((), SLanguage $ generateConceptStructure structureLanguage)]

refactoringEx2 :: Applicative f => MoveConceptRefactoring (f Int)
refactoringEx2 =
  MoveConceptRefactoring (pure 1) (fmap (+100)) (++"_new") DropMoved

refactorStructureWithLog refactoring lang =
  applyRefactoringLog Nothing (createLogFromRefactoring refactoring lang)
    (applyRefactoringToStructure refactoring lang)

repoEx2 =
  SRepository $ map ((,) () . SLanguage . (:[])) $ generateConceptStructure $
    languageEx2

repoEx2AfterRefactoring =
  SRepository $ map ((,) () . SLanguage . (:[])) $ generateConceptStructure $
    refactorStructureWithLog refactoringEx2 $ (map $ nodeIdMap Just) languageEx2

migrateUsing
  :: (Eq f, Eq pt, Show f, Show pt, Id nid) => MoveConceptRefactoring (Maybe Int)
     -> Node nid String String String String pt f
     -> Gen (Node nid String String String String pt f)
migrateUsing ref =
  applyScriptToRoot repoEx2
    (extractMigrationFromRefactoring ref $ (map $ nodeIdMap Just) languageEx2)

test2 = isValidModel coreRepository structureLanguage

test3 = isValidModel coreRepository languageEx2
test4 = isValidModel coreRepository $ refactorStructureWithLog refactoringEx2 $ (map $ nodeIdMap Just) languageEx2

test5 =
  isValidModel repoEx2 [nodeEx2]

test6 = once $ do
  migrated <- migrateUsing refactoringEx2 nodeEx2
  return $ isValidModel repoEx2AfterRefactoring [migrated]

testMigrationPreserveConsistency lang ref =
  let
    repo =
      SRepository $ map ((,) () . SLanguage . (:[])) $ generateConceptStructure $
        lang
    repoAfterRefactoring =
      SRepository $ map ((,) () . SLanguage . (:[])) $ generateConceptStructure $
        refactorStructureWithLog ref lang
  in
    forAllS (genRoot repo Nothing) $ (\initial -> do
      migrated <- migrateUsing ref initial
      return $ counterexample (ppShow initial ++ "\n" ++ ppShow migrated) $
        label ("root size = " ++ show (length $ descendants initial)) $
               isValidModel repo [initial]
          .&&. isValidModel repoAfterRefactoring [migrated])

test7 = testMigrationPreserveConsistency (take 1 $ (map $ nodeIdMap Just) languageEx2) refactoringEx2

test8 = testMigrationPreserveConsistency ((map $ nodeIdMap Just) languageEx2) refactoringEx2 {
  refactoringOptions = KeepMoved SupportSubconcepts
}

structureModule = Module {
  moduleId = "lang.structure",
  moduleVersion = 0,
  languageVersion = 0,
  moduleDependencies = [("lang.structure", 0)],
  usedLanguages = [("lang.structure", 0)],
  moduleContent = structureLanguage,
  migrations = [],
  refactoringLogs = []
}

moduleEx2 = Module {
  moduleId = "ex2",
  moduleVersion = 0,
  languageVersion = 0,
  moduleDependencies = [("ex2", 0)],
  usedLanguages = [("lang.structure", 0)],
  moduleContent = languageEx2,
  migrations = [],
  refactoringLogs = []
}

projectEx2 = map (moduleContentMap (nodeIdMap (:[0]))) [structureModule, moduleEx2]

test9  = isValidProject [structureModule]
test10 = isValidProject projectEx2

test11 = expectFailure $
  isValidProject $ refactorProject ("ex2",refactoringEx2) projectEx2

test12 = propertyOnce $ do
  let refactored = refactorProject ("ex2",refactoringEx2) projectEx2
  migratedFully <- migrateFully refactored
  return $ isValidProject migratedFully

demo13 =
  pPrint =<<
    (generateSS $ resizeSS 2 $
      genModules (const (arbitrary::Gen String)) (SRepository $ zip ["ex2"] (map (SLanguage . (:[])) $ generateConceptStructure $ languageEx2)) [])

-- Here we demonstrate that simply dropping old concept breaks dependent project consistency when is it opened on
-- new language version but not migrated yet
test15 = TestCase {
  testCaseName = "test15",
  testCaseStatic = refactoringEx2 :: MoveConceptRefactoring [Int],
  testCaseGenerated = \static ->
    let
      repo = compileRepo [moduleEx2]
    in
      withIds (0::Int) $ withModuleId $ genModules (const (arbitrary::Gen String)) repo [],
  testCaseInitial = \static generated -> [(map . moduleContentMap . nodeIdMap) (:[]) [structureModule, moduleEx2], (map . moduleContentMap . nodeIdMap) (:[0])generated],
  testCaseProcess = \static project -> do
    let refactored = runIdentity $ withProjects [0] (Identity . refactorProject ("ex2", static)) project
    migratedLang0 <- withProjects [0] migrateFully refactored
    migrated <- withProjects [0,1] migrateFully migratedLang0
    return [("initial", project), ("refactored", refactored), ("migrated base project", migratedLang0), ("migrated derived projects", migrated)],
  testCaseCheck = \[("initial", project), ("refactored", refactored), ("migrated base project", migratedLang0), ("migrated derived projects", migrated)] ->
    [
      ("initial projects should be valid", isValidProject (concat project)),
      ("dependent projects opened with updated base should be valid", isValidProjectAllowPending (concat migratedLang0)),
      ("final projects should be valid and fully migrated", isValidProject (concat migrated))
    ]
}

-- Here we demonstrate that current implementation of keeping old concepts breaks link/reference target concept consistency
test16 = test15 {
  testCaseName = "test16",
  testCaseStatic = refactoringEx2 {
    refactoringOptions = KeepMoved SupportSubconcepts
  } :: MoveConceptRefactoring [Int]
}

-- Here we improve keeping old concept so that links and reference targets remain consistent
test17 = test15 {
  testCaseName = "test17",
  testCaseStatic = refactoringEx2 {
    refactoringOptions = KeepMoved SupportSubconceptsImproved
  } :: MoveConceptRefactoring [Int]
}

-- Here we show (for particular concept set) that migrateManually option does preserve consistency after the same improvement
test18 = test15 {
  testCaseName = "test18",
  testCaseStatic = refactoringEx2 {
    refactoringOptions = KeepMoved MigrateManuallyImproved
  } :: MoveConceptRefactoring [Int]
}

-- This is a technical test showing that languages can be generated without cyclic extending concepts
test19 = test15 {
  testCaseName = "test19",
  testCaseGenerated = \static ->  withIds ([0]::[Int]) $ withModuleId $ do
    let structureRepo = compileRepo [structureModule]
    let staticLanguages = (map . moduleContentMap . nodeIdMap) (:[1]) [moduleEx2]
    languages' <- genModules genStructureProperty structureRepo staticLanguages
    languagesWithStatic <- (lift . lift . removeCyclicDependencies) (staticLanguages ++ languages')
    return [drop (length staticLanguages) languagesWithStatic],
  testCaseInitial = \static [languages] -> [(map . moduleContentMap . nodeIdMap) (:[0]) [structureModule], (map . moduleContentMap . nodeIdMap) (:[1]) [moduleEx2], languages],
  testCaseProcess = \static project -> return [("initial", project)],
  testCaseCheck = \[("initial", project)] ->
    [
      ("initial projects should be valid", isValidProject (concat project)),
      ("all languages should be valid", isValidLanguage (compileRepo $ (project !! 1) ++ (project !! 2)))
    ]
}

-- This is also a technical test showing that languages can be generated without cyclic extending concepts, and instances of such languages also can be generated
test20 = TestCase {
  testCaseName = "test20",
  testCaseStatic = (),
  testCaseGenerated = \static ->  withIds ([0]::[Int]) $ withModuleId $ do
    let structureRepo = compileRepo [structureModule]
    let staticLanguages = (map . moduleContentMap . nodeIdMap) (:[1]) [moduleEx2]
    languages' <- genModules genStructureProperty structureRepo staticLanguages
    languagesWithStatic <- (lift . lift . removeCyclicDependencies) (staticLanguages ++ languages')
    let repo = compileRepo languagesWithStatic
    instanceModules <- genModules (const (arbitrary::Gen String)) repo []
    return [drop (length staticLanguages) languagesWithStatic, instanceModules],
  testCaseInitial = \static [languages, instances] ->
    [(map . moduleContentMap . nodeIdMap) (:[0]) [structureModule], (map . moduleContentMap . nodeIdMap) (:[1]) [moduleEx2], languages, instances],
  testCaseProcess = \static project -> do
    return [("initial", project)],
  testCaseCheck = \[("initial", project)] ->
    [
      ("initial projects should be valid", isValidProject (concat project))
    ]
}

-- this test checks that migrating with inducing logs in subconcepts leads to fully migrated project
test21 = TestCase {
  testCaseName = "test21",
  testCaseStatic = refactoringEx2 {
    moveFrom = [1, 1],
    refactoringOptions = KeepMoved SupportSubconceptsImproved
  } :: MoveConceptRefactoring [Int],
  testCaseGenerated = \static ->  withIds ([0]::[Int]) $ withModuleId $ do
    let structureRepo = compileRepo [structureModule]
    let staticLanguages = (map . moduleContentMap . nodeIdMap) (:[1]) [moduleEx2]
    languages' <- genModules genStructureProperty structureRepo staticLanguages
    languagesWithStatic <- (lift . lift . removeCyclicDependencies) (staticLanguages ++ languages')
    let repo = compileRepo languagesWithStatic
    instanceModules <- genModules (const (arbitrary::Gen String)) repo []
    return [drop (length staticLanguages) languagesWithStatic, instanceModules],
  testCaseInitial = \static [languages, instances] ->
    [(map . moduleContentMap . nodeIdMap) (:[0]) [structureModule], (map . moduleContentMap . nodeIdMap) (:[1]) [moduleEx2], languages, instances],
  testCaseProcess = \static project -> do
      let refactored = runIdentity $ withProjects [0,1,2] (Identity . refactorProject ("ex2", static)) project
      migratedLang0 <- withProjects [0,1,2] migrateFully refactored
      migrated <- withProjects [0,1,2,3] migrateFully migratedLang0
      return [("initial", project), ("refactored", refactored), ("migrated base project", migratedLang0), ("migrated derived projects", migrated)],
  testCaseCheck = \[("initial", project), ("refactored", refactored), ("migrated base project", migratedLang0), ("migrated derived projects", migrated)] ->
    [
      ("initial projects should be valid", isValidProject (concat project)),
      ("dependent projects opened with updated base should be valid", isValidProjectAllowPending (concat migratedLang0)),
      ("final projects should be valid without pending migrations", isValidProject (concat migrated)),
      ("final projects should be fully migrated", isMigratedProject (concat migrated))
    ]
}