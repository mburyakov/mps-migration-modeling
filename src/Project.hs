module Project where

import Data.Maybe
import Data.List
import Control.Monad
import Data.Function
import Data.List.Split

import Util.Util

import Model
import Migration
import Branch
import Metalang


data Module mid nid cid pid rid lid pt f = Module {
  moduleId :: mid,
  moduleVersion :: Int,
  languageVersion :: Int,
  moduleDependencies :: [(mid, Int)],
  usedLanguages :: [(mid, Int)],
  moduleContent :: [Node nid cid pid rid lid pt f],
  -- we assume that metalanguages are not subjects of migration, so migration aspect is affected only by refactoring logs
  migrations :: [Node nid cid pid rid lid pt f],
  refactoringLogs :: [RefactoringLog nid]
} deriving Show

resolveModule project mid =
  fromSingleChecking (filter (\m ->
    moduleId m == mid
  ) project) ("Searched module with id " ++ show mid)

isValidModule (SRepository conceptsByLang) project mdule = do
  let conceptsOfUsedLanguages = map (langConcepts . fromJust . flip lookup conceptsByLang) (map fst $ usedLanguages mdule)
  let repo = SRepository $ map ((,) () . SLanguage) conceptsOfUsedLanguages
  visibleModules <- mapM (resolveModule project) (map fst (moduleDependencies mdule))
  mapM_ (isValidRoot repo (concatMap moduleContent visibleModules)) (moduleContent mdule)
  mapM_ (isValidRoot migrationLanguageRepo (concatMap moduleContent visibleModules)) (migrations mdule)

isValidProjectAllowPending project = do
  let allUsedLanguages = nub $ map fst $ concatMap usedLanguages project
  resolvedUsedLanguages <- mapM (resolveModule project) allUsedLanguages
  let conceptsByLang = compileRepo resolvedUsedLanguages
  let concepts = concat $ map (langConcepts . snd) $ repoLanguages conceptsByLang
  checkWithMessage ("duplicated concept ids") $
    allUnique (map conceptId concepts)
  checkWithMessage ("duplicated node ids") $
    allUnique (map nodeId (concatMap descendants $ concatMap moduleContent project))
  mapM_ (\m -> isValidModule conceptsByLang project m) project

isMigratedProject project = do
  let allUsedLanguages = nub $ map fst $ concatMap usedLanguages project
  resolvedUsedLanguages <- mapM (resolveModule project) allUsedLanguages
  let repo = compileRepo resolvedUsedLanguages
  migr <- mapM (compileMigration project) project
  let checkFun = sequence . mapM (\ms -> checkMigrated repo ms) (concat migr)
  mapM_ checkFun $ concatMap descendants $ concatMap moduleContent project

isValidProject project = do
  isValidProjectAllowPending project
  mapM_ (isMigratedModule project) project

isMigratedModule project m = do
  mapM_ isLangVersionCoincides (usedLanguages m)
  mapM_ isDepVersionCoincides (moduleDependencies m)
    where
      isDepVersionCoincides (depRef, version) = do {
        dep <- resolveModule project depRef;
        checkWithMessage ("module " ++ show (moduleId m) ++ " depends on version " ++ show version ++ " of module " ++ show (moduleId dep) ++ " while it now has version " ++ show (moduleVersion dep)) $
          version == moduleVersion dep
      }
      isLangVersionCoincides (langRef, version) = do {
        lang <- resolveModule project langRef;
        checkWithMessage ("module " ++ show (moduleId m) ++ " uses version " ++ show version ++ " of language " ++ show (moduleId lang) ++ " while it now has version " ++ show (languageVersion lang)) $
          version == languageVersion lang
      }


refactorProject :: (Id mid, BlackId nid) => (mid, MoveConceptRefactoring nid) -> [Module mid nid String String String String String ()] -> [Module mid nid String String String String String ()]
refactorProject (mRef, refactoring) project =
  map (\m ->
    if moduleId m == mRef then
      m {
        moduleContent = applyRefactoringToStructure refactoring (moduleContent m),
        refactoringLogs = createLogFromRefactoring refactoring (moduleContent m) : refactoringLogs m,
        migrations = extractMigrationFromRefactoringSource refactoring (moduleContent m) : migrations m,
        moduleVersion = moduleVersion m + 1,
        languageVersion = languageVersion m + 1
      }
    else
      m
  ) project

migrateProjectStepLog (logContainer, v) project = do
  logModule <- resolveModule project logContainer
  let refLog = reverse (refactoringLogs logModule) !! v
  mapM (\m ->
    case lookup logContainer (moduleDependencies m) of
      Just existingVersion ->
        if (existingVersion == v) then do
          dependencies <- mapM (resolveModule project . fst) $ moduleDependencies m
          let migr = applyRefactoringLog (Just (concatMap moduleContent dependencies)) refLog (migrations m)
          return $ seq logModule $ m {
            moduleDependencies = map (\(depId, depVer) ->
              if depId == logContainer then
                (logContainer, existingVersion + 1)
              else
                (depId, depVer)
              ) $ moduleDependencies m,
            moduleContent = applyRefactoringLog Nothing refLog (moduleContent m),
            migrations = migr,
            languageVersion = languageVersion m + (length migr - length (migrations m))
          }
        else
          fail $ "cannot apply refactoringLog to module " ++ show m ++ ", wrong version"
      Nothing ->
        return m
   ) project

moduleOwnRepo project aModule = do
  let SRepository fullRepo = compileRepo project
  let conceptsOfUsedLanguages = map (langConcepts . fromJust . flip lookup fullRepo) (map fst $ usedLanguages aModule)
  return $ SRepository $ map ((,) () . SLanguage) conceptsOfUsedLanguages

maxProjectId project =
  max $ map nodeId (concatMap descendants $ concatMap moduleContent project)

migrateProjectStepLang (lang, v) project = do
  langModule <- resolveModule project lang
  allMigrations <- compileMigration project langModule
  let migration = reverse allMigrations !! v
  mapM (\m ->
    case lookup lang (usedLanguages m) of
      Just existingVersion ->
        if (existingVersion == v) then do
          myRepo <- moduleOwnRepo project m
          migratedContent <- mapM (applyScriptToRoot myRepo migration) (moduleContent m)
          return $ seq langModule $ m {
            usedLanguages = map (\(langId, langVer) ->
              if langId == lang then
                (lang, existingVersion + 1)
              else
                (langId, langVer)
              ) $ usedLanguages m,
            moduleContent = migratedContent
          }
        else
          fail $ "cannot apply migration to module " ++ show m ++ ", wrong version"
      Nothing ->
        return m
   ) project

migrateFully project = migrateFullyLimit (-1) project

withProjects indices f projects = do
  let included = map (\i -> projects !! i) indices
  processed <- f $ concat included
  let processedMap = map (\m -> (moduleId m, m)) processed
  let selected = (map . map) (\m -> fromMaybe m $ lookup (moduleId m) processedMap) projects
  return selected

compileRepo modules = SRepository $ zip (map moduleId modules) languages
  where
    concepts = generateConceptStructure $ concatMap moduleContent modules
    languages = map SLanguage $ splitPlaces (map (length . moduleContent) modules) concepts

compileMigration project langModule = do
  dependencies <- mapM (resolveModule project . fst) $ moduleDependencies langModule
  return $ map (generateMigration (concatMap moduleContent dependencies)) (migrations langModule)

migrateFullyLimit 0 project = return project
migrateFullyLimit limit project = do
  let allModuleVersions = map (\m -> (moduleId m, moduleVersion m)) project
  let allLangVersions = map (\m -> (moduleId m, languageVersion m)) project
  let notMigratedDepVersions = nub (concatMap moduleDependencies project) \\ allModuleVersions
  let notMigratedLangVersions = nub (concatMap usedLanguages project) \\ allLangVersions
  if (map length (groupBy ((==) `on` fst) notMigratedDepVersions) \\ [1]) /= [] then fail "project is partially migrated" else return ()
  if (map length (groupBy ((==) `on` fst) notMigratedLangVersions) \\ [1]) /= [] then fail "project is partially migrated" else return ()
  if notMigratedDepVersions == [] then
    if notMigratedLangVersions == [] then
      return project
    else do
      (lang, ver) <- variants "choosing migration to be executed first" notMigratedLangVersions
      nextStep <- migrateProjectStepLang (lang, ver) project
      migrateFullyLimit (limit - 1) nextStep
  else do
    (logContainer, ver) <- variants "choosing refactoring log to be executed first" notMigratedDepVersions
    nextStep <- migrateProjectStepLog (logContainer, ver) project
    migrateFullyLimit (limit - 1) nextStep

moduleContentMap
  :: (Node nid1 cid pid rid lid pt f1
      -> Node nid2 cid pid rid lid pt f2)
     -> Module mid nid1 cid pid rid lid pt f1
     -> Module mid nid2 cid pid rid lid pt f2
moduleContentMap f m | null (refactoringLogs m) = m {
  moduleContent = map f (moduleContent m),
  migrations = map f (migrations m),
  refactoringLogs = []
}